DELETE FROM persona;

INSERT INTO persona (nombreConApellido, padre) VALUES
('Homer Jay Simpson', NULL),
('Marge Jacqueline Simpson', NULL),
('Bartholomew JoJo Simpson', 1),
('Lisa Marie Simpson', 2),
('Margaret Simpson', 2);