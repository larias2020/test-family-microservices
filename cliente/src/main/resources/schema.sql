# noinspection SqlNoDataSourceInspectionForFile

CREATE TABLE IF NOT EXISTS persona (
  id BIGINT NOT NULL AUTO_INCREMENT,
  nombreConApellido VARCHAR(32) NOT NULL,
  padre INT(11),
  PRIMARY KEY (id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;