package com.arias.cliente.controller;

import com.arias.cliente.DTO.PersonaDTO;
import com.arias.cliente.Entities.Persona;
import com.arias.cliente.Services.PersonaService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.ResponseEntity;

import java.text.ParseException;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * The type Persona controller.
 * @author Luis Arias
 */
@RestController
@RequestMapping("/api/v1/persona")
public class PersonaController {

    @Autowired
    private PersonaService personaService;

    @Autowired
    private ModelMapper modelMapper;


    /**
     * Gets all details.
     *
     * @param id the id
     * @return the all details
     */
    @GetMapping("/{id}")
    public ResponseEntity<PersonaDTO> getAllDetails(@PathVariable("id") Long id) {
        return personaService.encontrarPersonabyId(id).map(mapToPersonaDTO).map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
    }

    /**
     * Gets all siblings.
     *
     * @param id the id
     * @return the all siblings
     */
    @GetMapping("/{id}/hijos")
    public ResponseEntity<Set<PersonaDTO>> getAllSiblings(@PathVariable("id") Long id) {
        Optional<Persona> optionalPersona = personaService.encontrarHijosByID(id);
        return  optionalPersona.map(findSiblings).map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    /**
     * Create persona persona dto.
     *
     * @param personaDTO the persona dto
     * @param padreId    the padre id
     * @return the persona dto
     * @throws ParseException the parse exception
     */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public PersonaDTO createPersona(@RequestBody PersonaDTO personaDTO, @RequestParam Long padreId ) throws ParseException {
        Persona personaToService = convertToEntity(personaDTO, padreId);
        Persona personaFromService = personaService.createPersona(personaToService);
        return convertToDto(personaFromService);
    }

    /**
     * Update post.
     *
     * @param personaDTO the persona dto
     * @param padreId    the padre id
     * @throws ParseException the parse exception
     */
    @PutMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void updatePost(@RequestBody PersonaDTO personaDTO,@RequestParam Long padreId) throws ParseException {
        Persona persona = convertToEntity(personaDTO,padreId);
        personaService.updatePersona(persona);
    }

    /**
     * Delete persona response entity.
     *
     * @param id the id
     * @return the response entity
     */
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Long> deletePersona(@PathVariable Long id) {
        boolean isRemoved = personaService.delete(id);
        if (!isRemoved) return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(id, HttpStatus.OK);
    }


    private PersonaDTO convertToDto(Persona persona) {
        PersonaDTO postDto = persona.customShow(mapToPersonaDTO);
        return postDto;
    }


    private Persona convertToEntity(PersonaDTO personaDTO, Long padre) throws ParseException {
        Persona persona = modelMapper.map(personaDTO, Persona.class);
        if(padre!=null){
            Persona padrePersona = new Persona();
            padrePersona.setId(padre);
            persona.setPadre(padrePersona);
        }
        return  persona;
    }

    private Function<Persona, PersonaDTO> mapToPersonaDTO = p -> PersonaDTO.builder().id(p.getId()).nombreConApellido(p.getNombreConApellido()).padre(p.getPadre()).hijos(p.getHijos()).build();

    private Function<Persona, Set<PersonaDTO>> findSiblings = person -> person.getHijos().stream()
            .map(p -> PersonaDTO.builder().id(p.getId()).nombreConApellido(p.getNombreConApellido()).build()).collect(Collectors.toSet());
}
