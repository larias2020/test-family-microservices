package com.arias.cliente.Config;

import com.arias.cliente.Services.PersonaService;
import com.arias.cliente.Services.impl.PersonaServiceImpl;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * The type Spring config.
 * @author Luis Arias
 */
@Configuration
public class SpringConfig {
    /**
     * Persona service persona service.
     *
     * @return the persona service
     */
    @Bean
    public PersonaService personaService() {
        return new PersonaServiceImpl();
    }

    /**
     * Model mapper model mapper.
     *
     * @return the model mapper
     */
    @Bean
    public ModelMapper modelMapper(){ return new ModelMapper();}
}
