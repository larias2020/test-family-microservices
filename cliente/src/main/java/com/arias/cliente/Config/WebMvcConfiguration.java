package com.arias.cliente.Config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import com.arias.cliente.component.RequesTimeInterceptor;


/**
 * The type Web mvc configuration.
 * @author Luis Arias
 */
@Configuration
public class WebMvcConfiguration implements WebMvcConfigurer {

    @Autowired
    @Qualifier("requestTimeInterceptor")
    private RequesTimeInterceptor requesTimeInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(requesTimeInterceptor);
    }

}