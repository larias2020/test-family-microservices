package com.arias.cliente.Repositories;

import com.arias.cliente.Entities.Log;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

/**
 * The interface Log repository.
 * @author Luis Arias
 */
@Repository("logRepository")
public interface LogRepository extends JpaRepository<Log, Serializable> {
}
