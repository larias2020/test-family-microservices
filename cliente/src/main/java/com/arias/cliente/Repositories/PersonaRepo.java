package com.arias.cliente.Repositories;

import com.arias.cliente.Entities.Persona;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * The interface Persona repo.
 * @author Luis Arias
 */
@Repository
public interface PersonaRepo extends JpaRepository<Persona,Long> {
}
