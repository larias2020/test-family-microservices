package com.arias.cliente.Entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

/**
 * The type Log.
 * @author Luis Arias
 */
@Entity
@Table(name = "log")
public class Log {

    @Id
    @Getter
    @Setter
    @GeneratedValue
    @Column(name = "id")
    private int id;

    @Column(name = "date")
    @Getter
    @Setter
    private Date date;

    @Column(name = "details")
    @Getter
    @Setter
    private String details;

    @Column(name = "username")
    @Getter
    @Setter
    private  String username;

    @Column(name = "url")
    @Getter
    @Setter
    private  String url;

    /**
     * Instantiates a new Log.
     *
     * @param date     the date
     * @param details  the details
     * @param username the username
     * @param url      the url
     */
    public Log( Date date, String details, String username, String url) {
        super();
        this.date = date;
        this.details = details;
        this.username = username;
        this.url = url;
    }

    /**
     * Instantiates a new Log.
     */
    public Log(){}
}
