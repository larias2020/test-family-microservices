package com.arias.cliente.Entities;


import com.arias.cliente.DTO.PersonaDTO;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;
import java.util.function.Function;

/**
 * The type Persona.
 * @author Luis Arias
 */
@Entity
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties({"hibernate_lazy_initializer", "handler"})
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Persona {

    @Id
    @Getter
    @Setter
    @EqualsAndHashCode.Include
    @GeneratedValue(strategy=GenerationType.SEQUENCE)
    private Long id;

    @Getter
    @Setter
    private String nombreConApellido;

    @ManyToOne(fetch = FetchType.LAZY)
    @Getter
    @Setter
    @org.hibernate.annotations.ColumnDefault("")
    private Persona padre;

    @OneToMany(fetch = FetchType.LAZY,mappedBy = "padre")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Setter
    private Set<Persona> hijos;

    /**
     * Gets hijos.
     *
     * @return the hijos
     */
    @JsonIgnore
    public Set<Persona> getHijos() {
        return hijos;
    }


    /**
     * Custom show persona dto.
     *
     * @param fun the fun
     * @return the persona dto
     */
    public  PersonaDTO customShow(Function<Persona, PersonaDTO> fun){
        return fun.apply(this);
    }
}
