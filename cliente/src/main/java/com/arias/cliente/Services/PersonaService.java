package com.arias.cliente.Services;

import com.arias.cliente.Entities.Persona;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * The interface Persona service.
 * @author Luis Arias
 */
@Service
public interface PersonaService {
    /**
     * Encontrar personaby id optional.
     *
     * @param id the id
     * @return the optional
     */
    Optional<Persona> encontrarPersonabyId(Long id );

    /**
     * Encontrar hijos by id optional.
     *
     * @param id the id
     * @return the optional
     */
    Optional<Persona> encontrarHijosByID(Long id);

    /**
     * Create persona persona.
     *
     * @param persona the persona
     * @return the persona
     */
    Persona createPersona(Persona persona);

    /**
     * Update persona boolean.
     *
     * @param persona the persona
     * @return the boolean
     */
    boolean updatePersona(Persona persona);

    /**
     * Delete boolean.
     *
     * @param id the id
     * @return the boolean
     */
    boolean delete(Long id);
}
