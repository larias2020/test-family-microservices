package com.arias.cliente.Services.impl;

import com.arias.cliente.Entities.Persona;
import com.arias.cliente.Repositories.PersonaRepo;
import com.arias.cliente.Services.PersonaService;
import org.springframework.beans.factory.annotation.Autowired;


import java.util.Optional;

/**
 * The type Persona service.
 * @author Luis Arias
 */
public class PersonaServiceImpl implements PersonaService {

    @Autowired
    private PersonaRepo personaRepo;

    @Override
    public Optional<Persona> encontrarPersonabyId(Long id) {
        return personaRepo.findById(id);
    }

    @Override
    public Optional<Persona> encontrarHijosByID(Long id) {
        return personaRepo.findById(id);
    }

    @Override
    public Persona createPersona(Persona persona) {
        return personaRepo.save(persona);
    }

    @Override
    public boolean updatePersona(Persona persona){
        if(personaRepo.existsById(persona.getId())){personaRepo.delete(persona);return true;} return false;
    }

    @Override
    public boolean delete(Long id) {
        if(personaRepo.existsById(id)){personaRepo.deleteById(id);return true;} return false;
    }
}


