package com.arias.familyproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class FamilyprojectApplication {

    public static void main(String[] args) {
        SpringApplication.run(FamilyprojectApplication.class, args);
    }

}
